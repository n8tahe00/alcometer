import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  genders = [];
  bottles = [];
  times = [];
  weight: number;
  gender: string;
  promilles: number;
  time: number;
  bottle: number;
  litres: number;
  grams: number;
  burning: number;
  gramsLeft: number;

  constructor() {

}

ngOnInit() {
  this.genders.push('Male');
  this.genders.push('Female');
  this.times.push(1);
  this.times.push(2);
  this.times.push(3);
  this.times.push(4);
  this.times.push(5);
  this.times.push(6);
  this.bottles.push(1);
  this.bottles.push(2);
  this.bottles.push(3);
  this.bottles.push(4);
  this.bottles.push(5);
  this.bottles.push(6);

  // set initial values
  this.weight = 89;
  this.gender = 'Male';
  this.time = 1;
  this.bottle = 3;
  }

calculate() {
  this.litres = this.bottle * 0.33;
  this.grams = this.litres * 8 * 4.5;
  this.burning = this.weight / 10;
  this.gramsLeft = this.grams - (this.burning * this.time)

  if (this.gender === 'Male') {
    this.promilles = this.gramsLeft / (this.weight * 0.7);
    this.promilles = Math.round(this.promilles * 100) / 100;
  } else if (this.gender === 'Female') {
    this.promilles = this.gramsLeft / (this.weight * 0.6);
    this.promilles = Math.round(this.promilles * 100) / 100;
  } else {

  }
}

}